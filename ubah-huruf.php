<?php
function ubah_huruf($string){
//kode di sini
    $array = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
    "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"];
    $extend = "";

    if(!is_string($string)) {
        return "Masukkan Kata";
    } else {
        foreach(str_split($string) as $huruf) {
            for($i = 0; $i < count($array); $i++) {
                if(strtolower($huruf) == $array[$i]) {
                    $extend .= $array[$i+1];
                }
            }
        }
        return $extend ."<br>";
    }
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>